FROM golang:alpine AS build
RUN apk --no-cache add gcc g++ make git sqlite
WORKDIR /go/src/converter
COPY . .

RUN go get -u -v github.com/aws/aws-sdk-go
RUN GOOS=linux go build -ldflags="-s -w" -o ./bin/converter .

FROM alpine:latest
RUN apk --no-cache add ca-certificates ffmpeg bash
WORKDIR /usr/bin
COPY --from=build /go/src/converter/bin .
COPY --from=build /go/src/converter/converter_config.json .
