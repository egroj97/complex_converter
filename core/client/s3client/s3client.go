package s3client

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	"complex_converter/config"
)

// S3Client stores the S3 downloader and uploader managers with the bucket's
// name over which perform operations.
type S3Client struct {
	uploader   *s3manager.Uploader
	downloader *s3manager.Downloader
	bucket     string
}

// New takes the configuration for the client and sets up all necessary AWS
// credentials to establish the communication with the platform.
//
// Returns a pointer to the instance of the S3Client structure and an error.
func New(config config.Configuration) (*S3Client, error) {
	sess := session.Must(session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(
			config.AWSAccessKey,
			config.AWSSecretAccessKey,
			"",
		),
	}))

	region, err := s3manager.GetBucketRegion(
		aws.BackgroundContext(),
		sess,
		config.AWSBucket,
		"us-west-2",
	)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok && aerr.Code() == "NotFound" {
			return nil, fmt.Errorf(
				"unable to find bucket %s's region not found\n",
				config.AWSBucket,
			)
		}
		return nil, err
	}

	sess = session.Must(session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(
			config.AWSAccessKey,
			config.AWSSecretAccessKey,
			"",
		),
		Region: aws.String(region),
	}))

	client := new(S3Client)
	client.uploader = s3manager.NewUploader(sess)
	client.downloader = s3manager.NewDownloader(sess)
	client.bucket = config.AWSBucket

	return client, nil
}

// DownloadFile receives the name of the resource to be retrieved from the
// platform and returns the path for the file downloaded and an error.
func (c *S3Client) DownloadFile(resourceName string) (string, error) {
	out, err := ioutil.TempFile("/", resourceName)
	if err != nil {
		return "", err
	}
	defer out.Close()

	_, err = c.downloader.Download(out, &s3.GetObjectInput{
		Bucket: aws.String(c.bucket),
		Key:    aws.String(resourceName),
	})
	if err != nil {
		return "", fmt.Errorf("failed to download file, %v", err)
	}

	return out.Name(), err
}

// UploadFile receives the path for the transcoded file to upload it to the
// platform and returns an error.
func (c *S3Client) UploadFile(filePath string) error {
	f, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("failed to open file %q, %v", filePath, err)
	}

	// Upload the file to S3.
	_, err = c.uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(c.bucket),
		Key:    aws.String(filepath.Base(filePath)),
		Body:   f,
	})
	if err != nil {
		return fmt.Errorf("failed to upload file, %v", err)
	}

	return nil
}
