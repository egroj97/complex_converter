package client

import (
	"fmt"

	"complex_converter/config"
	"complex_converter/core/client/s3client"
)

// Client is an interface that only defines two methos.
//
// Methods:
// DownloadFile receives the name of the resource to be fetch for the platform.
// returns the path to the downloaded file and an error.
//
// UploadFile receives the path of the file to be sent to the platform.
// returns an error
//
type Client interface {
	DownloadFile(resourceName string) (string, error)
	UploadFile(filePath string) error
}

// New instances a type from the defined clients that comply with the Client
// interface of the platform defined by option with the given configuration.
//
// Returns an instance of a type that complies with the Client interface and
// an error.
//
func New(option string, config config.Configuration) (Client, error) {
	switch option {
	case "s3":
		return s3client.New(config)
	}
	return nil, fmt.Errorf("platform not supported")
}
