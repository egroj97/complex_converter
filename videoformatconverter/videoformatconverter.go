package videoformatconverter

import (
	"fmt"

	"complex_converter/videoformatconverter/ffmpeg"
)

// VideoFormatConverter is an interface that defines one method.
//
// Methods:
// ConvertVideo transcodes the given file path to the given extension and with
// the given encoding.
//
// Returns the path to the transcoded file and an error.
//
type VideoFormatConverter interface {
	ConvertVideo(to, encoding, path string) (string, error)
}

// New instances a type from the defined converters that comply with the
//  VideoFormatConverter interface of the engine defined.
//
// Returns an instance of a type that complies with the VideoFormatConverter
// interface and an error.
//
func New(engine string) (VideoFormatConverter, error) {
	switch engine {
	case "ffmpeg":
		return ffmpeg.New()
	}
	return nil, fmt.Errorf("transcoding engine not supported")
}
