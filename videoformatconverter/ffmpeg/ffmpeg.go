package ffmpeg

import (
	"os/exec"
	"strings"
)

// Cmd is an empty structure.
type Cmd struct {
}

// New returns a pointer to an instance of the Cmd type.
func New() (*Cmd, error) {
	return new(Cmd), nil
}

// ConvertVideo transcodes the given file path to the given extension and with
// the given encoding library. For this the ffmpeg command-line tool is called
// from the program.
//
// Returns the path to the transcoded file and an error.
//
func (c *Cmd) ConvertVideo(to, encoding, path string) (string, error) {
	parts := strings.Split(path, ".")
	from := parts[len(parts)-1]
	output := strings.ReplaceAll(path, from, to)
	cmd := exec.Command(
		"ffmpeg",
		"-i",
		path,
		"-c:a",
		"copy",
		"-c:v",
		encoding,
		output,
	)

	err := cmd.Run()
	if err != nil {
		return "", err
	}

	return output, nil
}
