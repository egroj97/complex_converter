package config

// Configuration stores all necessary information for the usage of the
// application.
// @TODO It should be changed to an interface so that it can be interchanged with
//  other types of clients.
type Configuration struct {
	EndpointURL        string `json:"endpoint_url"`
	AWSAccessKey       string `json:"aws_access_key_id"`
	AWSSecretAccessKey string `json:"aws_secret_access_key"`
	AWSBucket          string `json:"aws_bucket"`
	AWSRegion          string `json:"aws_region"`
}

var (
	// DefaultConfiguration contains a default configuration for the application
	// if needed.
	DefaultConfiguration = Configuration{
		EndpointURL:        "",
		AWSAccessKey:       "",
		AWSSecretAccessKey: "",
		AWSBucket:          "",
		AWSRegion:          "",
	}

	// EnvironmentFilePath stores the path to the configuration file which can be
	// used to try different configurations without having to modify the source
	// code.
	EnvironmentFilePath = "converter_config.json"
)
