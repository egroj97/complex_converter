package main

import (
	"flag"
	"log"

	"complex_converter/app"
)

func main() {
	to := *flag.String(
		"to",
		"mp4",
		"extension of the target file",
	)

	codec := *flag.String(
		"codec",
		"libx264",
		"codec to be used on the video conversion",
	)

	engine := *flag.String(
		"engine",
		"ffmpeg",
		"engine that leverages the transcoding of files",
	)

	platform := *flag.String(
		"platform",
		"s3",
		"platform for which the client will be created",
	)

	flag.Parse()

	application, err := app.New(engine, platform)
	if err != nil {
		log.Fatalf("Could not start application: %v", err)
	}

	err = application.Run(flag.Arg(0), codec, to)
	if err != nil {
		log.Fatalf("Could not convert video: %v", err)
	}
}
