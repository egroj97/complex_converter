package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"complex_converter/config"
	"complex_converter/core/client"
	vfc "complex_converter/videoformatconverter"
)

// Converter is the structure that stored the client and video complex_converter
// instances and which also has the necessary methods defined
//
type Converter struct {
	Client         client.Client
	VideoConverter vfc.VideoFormatConverter
}

// New receives the name of the engine to be used by the program, as well as the
// name of the client to be used for the file download and upload.
//
// Returns a pointer to a Converter structure and an error.
//
func New(engine, platform string) (*Converter, error) {
	var conf config.Configuration
	var err error
	app := new(Converter)

	if _, err := os.Stat(config.EnvironmentFilePath); err == nil {
		data, err := ioutil.ReadFile(config.EnvironmentFilePath)
		if err != nil {
			return nil, fmt.Errorf("could not read configuration file: %v", err)
		}

		err = json.NewDecoder(bytes.NewReader(data)).Decode(&conf)
		if err != nil {
			return nil, fmt.Errorf("could not decode configuration file: %v", err)
		}
	} else {
		conf = config.DefaultConfiguration
	}

	app.Client, err = client.New(platform, conf)
	if err != nil {
		return nil, err
	}

	app.VideoConverter, err = vfc.New(engine)
	if err != nil {
		return nil, err
	}

	return app, nil
}

// Run calls for the file download, transcoding and upload. Its expected to
// receive the name of the resource stored on internet, the encoding library to
// be used (as those defined by ffmpeg and the extension for the transcoded file.
func (c *Converter) Run(resourceName, codec, to string) error {
	var filePath string
	var err error

	filePath, err = c.Client.DownloadFile(resourceName)
	if err != nil {
		return err
	}

	convertedVideo, err := c.VideoConverter.ConvertVideo(to, codec, filePath)
	if err != nil {
		_ = os.Remove(filePath)
		return fmt.Errorf("file could not be transcoded %v", err)
	}

	err = c.Client.UploadFile(convertedVideo)
	if err != nil {
		return err
	}

	err = os.Remove(filePath)
	if err != nil {
		return err
	}

	err = os.Remove(convertedVideo)
	if err != nil {
		return err
	}

	return nil
}
